# Vet API

You will be building a basic API to start familiarizing yourself with the Spring Web annotations.

# Requirements

To begin fork and clone this repository.

You will be writing code in two files `controllers/PetController.java` and `models/Pet.java`.

## Pet.java

You will need to define the Pet model.

The pet model needs to contain the following properties:

- type -- examples: "dog", "cat", "bird"
- name -- examples: "Fido", "Tweety", "Garfield"
- age -- examples: 5, 3, 1
- ownerName -- examples: "Paul Matthews", "Patrick Cleary"

The model needs to contain:

- a constructor
- getters and setters for all the properties

## PetController.java

You will need to complete the `PetController.java` that handles requests at the base path `/pet`.

You will need to:

- add class annotations
- create an in memory class level pet object
- add a GET handler that returns the in memory class level pet object
- add a POST handler that receives incoming JSON and overwrites the in memory class level pet object adn returns a 201 status code

# Making Requests

You can make GET requests from a browser or by using curl example: `curl -X GET http://localhost:8080/pet`.

You can make POST requests using curl example: `curl -X POST http://localhost:8080/pet -H 'Content-Type: application/json' -d '{"type": "cat", "name": "Garfield", "ownerName": "Jon", "age": 9}'`

# Bonus Objectives

- change the in memory class level pet object to an ArrayList of Pet objects so multiple pets can be added with POST requests
- add an id property to the pet class
- add a GET method for the path `/pet/{id}` that uses the @PathVariable annotation that returns one singular pet object that matches the id path variable
- add a DELETE method for the path `/pet/{id}` that will remove that matches the id path variable from the ArrayList of Pet objects

# Git reminder

Remember to use feature branches for each requirement and bonus objective.

# Gradle reminder

You can run this project using the gradlewrapper from the terminal at the root of this project (the `vet-api/` directory) you can run `./gradlew bootRun` which will load the project.

You can close the webserver by pressing `cmd+c` or `ctrl+c` in the terminal running that is currently attached to the webserver.

If you are using Visual Studio Code and have the gradle extension you can also the ``bootRun`` command in the application section of Gradle Tasks.